// @ts-nocheck
/* eslint-disable node/no-unpublished-import */
import { renderHook } from '@testing-library/react-hooks';
import { waitFor } from '@testing-library/react';

// Instruments
import { useLogin } from '../useLogin';
import { createWrapper } from '../../tests/utils';

const fakeCredentials = {
    email:    'elonmask@gmail.com',
    password: 'whatsonmymind',
};

const fakeData = {
    data: 'some.jwt.string',
};

describe('useLoginHook', () => {
    it('should return data with token',
        async () => {
            const { result } = renderHook(() => useLogin(), {
                wrapper: createWrapper(),
            });

            const data = await waitFor(() => result.current.mutateAsync(fakeCredentials));
            await waitFor(() => expect(data)
                .toEqual(fakeData));
        });
});
