// @ts-nocheck
/* eslint-disable node/no-unpublished-import */
import { renderHook } from '@testing-library/react-hooks';
import { waitFor } from '@testing-library/react';
import { rest } from 'msw';

// Instruments
import { server } from '../../setupTests';
import { useResentComments } from '../useResentComments';
import { createWrapper } from '../../tests/utils';

const fakeData =  [
    {
        hash:   '3c3f7375-954b-4fa9-9402-ba856e004d4a',
        body:   'Классный пост',
        author: {
            hash: '3c3f7375-954b-4fa9-9402-ba856e004d4a',
            name: 'Chuck Norris',
        },
        created: '2022-07-10T14:29:26.467Z',
        post:    {
            hash: '3c3f7375-954b-4fa9-9402-ba856e004d4a',
        },
    },
];

describe('useResentCommentsHook', () => {
    it('should return array with comments',
        async () => {
            const { result } = renderHook(() => useResentComments(), {
                wrapper: createWrapper(),
            });

            await waitFor(() => expect(result.current.isSuccess)
                .toBe(true));

            expect(result.current.data)
                .toEqual(fakeData);
        });

    it('should throw an error', async () => {
        server.use(
            rest.get('*', (req, res, ctx) => {
                return res(
                    ctx.status(500),
                );
            }),
        );

        const { result } = renderHook(() => useResentComments(), {
            wrapper: createWrapper(),
        });

        await waitFor(() => expect(result.current.isError).toBe(true));
        expect(result.current.error).toBeDefined();
    });
});
