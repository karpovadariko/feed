import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { getAuthToken, getIsFetching } from '../lib/redux/selectors';

export const useNewPassword = () => {
    const navigate = useNavigate();
    const oldToken = localStorage.getItem('token');
    const storeToken = useSelector(getAuthToken);
    const isFetching = useSelector(getIsFetching);

    useEffect(() => {
        if (!isFetching && oldToken !== storeToken) {
            localStorage.setItem('token', storeToken);
            navigate('/feed');
        }
    }, [storeToken, isFetching]);
};
