// Core
import { AxiosError } from 'axios';
import { useEffect } from 'react';
import { useQuery } from 'react-query';
import { useNavigate } from 'react-router-dom';

// Instruments
import { api } from '../api';
import { authActions, uiActions } from '../lib/redux/actions';
import { useAppDispatch } from '../lib/redux/init/store';

export const useAuth = () => {
    const navigate = useNavigate();
    const { token } = api;
    const dispatch = useAppDispatch();
    const { refetch } = useQuery('auth', () => api.auth.auth(), {
        enabled:   false,
        retry:     false,
        onSuccess: () => {
            dispatch(authActions.setToken(token));
            dispatch(authActions.setIsAuthSuccess(true));
        },
        onError: (error: AxiosError) => {
            dispatch(uiActions.setErrorMessage(error?.response?.data?.message));
            dispatch(authActions.setIsAuthSuccess(false));
        },
    });

    useEffect(() => {
        // eslint-disable-next-line no-unused-expressions
        if (!token) {
            navigate('/login');

            return;
        }
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        refetch();
    }, [token]);
};
