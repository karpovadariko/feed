// Core
import { useQuery } from 'react-query';

// Instruments
import { api } from '../api';

export const usePostDetails = (postId: string) => {
    const { data, isFetching } = useQuery(['post', postId], () => api.posts.getPostById(postId));

    return (
        {
            data: data?.data ? data.data : [],
            isFetching,
        }
    );
};
