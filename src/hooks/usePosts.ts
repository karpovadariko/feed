// Core
import { useQuery } from 'react-query';

// Instruments
import { api } from '../api';

export const usePosts = () => {
    const { data, isFetching } = useQuery('posts', api.posts.fetch);

    return { data: Array.isArray(data) ? data : [], isFetching };
};
