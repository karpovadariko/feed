// Core
import { useQuery } from 'react-query';

// Instruments
import { api } from '../api';
import { profileActions, uiActions } from '../lib/redux/actions';
import { useAppDispatch } from '../lib/redux/init/store';
import { IProfileModel } from '../types/ProfileModel';

export const useProfile = () => {
    const dispatch = useAppDispatch();

    const { refetch } = useQuery<IProfileModel, Error>('profile', () => api.profile.fetch(), {
        enabled: false,

        onSuccess: (data) => {
            dispatch(profileActions.setUserName(data?.data?.name));
        },
        onError: (error) => {
            dispatch(uiActions.setErrorMessage(error?.message));
        },
    });

    return refetch;
};
