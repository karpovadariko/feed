// Core
import { useQuery } from 'react-query';

// Instruments
import { api } from '../api';

export const useResentComments = () => {
    const {
        data, isFetching, isError, error, isSuccess,
    } = useQuery('resentComments', api.posts.getComments);

    return {
        data: data?.data || [],
        isFetching,
        isError,
        isSuccess,
        error,
    };
};
