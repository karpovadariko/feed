// Core
import { useEffect } from 'react';
import { useMutation } from 'react-query';

// Instruments
import { api } from '../api';
import { IProfileFormShape } from '../components/forms/types';
import { profileActions } from '../lib/redux/actions';
import { useAppDispatch } from '../lib/redux/init/store';

export const useUpdateProfile = () => {
    const dispatch = useAppDispatch();

    const mutation = useMutation((profileInfo: IProfileFormShape) => {
        return api.profile.updateProfile(profileInfo);
    });

    useEffect(() => {
        const newData = mutation.data?.data;

        if (mutation.isSuccess && newData) {
            dispatch(profileActions.setUserName(newData.name));
        }
    }, [mutation.isSuccess]);


    return mutation;
};
