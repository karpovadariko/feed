// Core
import { useEffect } from 'react';
import { useMutation } from 'react-query';
import { useNavigate } from 'react-router-dom';

// Instruments
import { AxiosError } from 'axios';
import { api } from '../api';
import { uiActions, authActions } from '../lib/redux/actions';
import { ISignUp } from '../components/forms/types';
import { useAppDispatch } from '../lib/redux/init/store';

export const useSignUp = () => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const mutation = useMutation((userInfo: ISignUp) => {
        return api.auth.signup(userInfo);
    }, {
        onError: (error: AxiosError) => {
            const { response } = error;
            const errorMessage = response?.status === 401 ? 'Такой имейл уже существует' : 'Ошибка сервера, попробуйте позже';
            dispatch(uiActions.setErrorMessage(errorMessage));
        },
    });

    useEffect(() => {
        const token = mutation.data?.data;
        if (mutation.isSuccess && token) {
            localStorage.setItem('token', token);
            dispatch(authActions.setToken(token));
            navigate('/feed');
        }
    }, [mutation.isSuccess]);

    return mutation;
};
