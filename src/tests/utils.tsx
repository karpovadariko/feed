// @ts-nocheck
/* eslint-disable node/no-unpublished-import,import/no-extraneous-dependencies,no-console */
import { render } from '@testing-library/react';
import { rest } from 'msw';
import * as React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Provider } from 'react-redux';
import { store } from '../lib/redux/init/store';

export const handlers = [
    rest.get(
        '*/comments',
        (req, res, ctx) => {
            return res(
                ctx.status(200),
                ctx.json({
                    data: [
                        {
                            hash:   '3c3f7375-954b-4fa9-9402-ba856e004d4a',
                            body:   'Классный пост',
                            author: {
                                hash: '3c3f7375-954b-4fa9-9402-ba856e004d4a',
                                name: 'Chuck Norris',
                            },
                            created: '2022-07-10T14:29:26.467Z',
                            post:    {
                                hash: '3c3f7375-954b-4fa9-9402-ba856e004d4a',
                            },
                        },
                    ],
                }),
            );
        },
    ),
    rest.post(
        '*/login',
        (req, res, ctx) => {
            return res(
                ctx.status(200),
                ctx.json({
                    data: 'some.jwt.string',
                }),
            );
        },
    ),
];

const createTestQueryClient = () => new QueryClient({
    defaultOptions: {
        queries: {
            retry: false,
        },
    },
    logger: {
        log:   console.log,
        warn:  console.warn,
        error: () => {},
    },
});

export function renderWithClient(ui: React.ReactElement) {
    const testQueryClient = createTestQueryClient();
    const { rerender, ...result } = render(
        <Router>
            <Provider store = { store }>
                <QueryClientProvider client = { testQueryClient }>{ ui }</QueryClientProvider>
            </Provider>
        </Router>,
    );

    return {
        ...result,
        rerender: (rerenderUi: React.ReactElement) => rerender(
            <QueryClientProvider client = { testQueryClient }>{ rerenderUi }</QueryClientProvider>,
        ),
    };
}

export function createWrapper() {
    const testQueryClient = createTestQueryClient();

    return ({ children }: { children: React.ReactNode }) => (
        <Router>
            <Provider store = { store }>
                <QueryClientProvider client = { testQueryClient }>{ children }</QueryClientProvider>
            </Provider>
        </Router>
    );
}
