// Core
import axios, { AxiosResponse } from 'axios';
import {
    ILoginFormShape, ISignUp, IPostFormShape, ICommentFormShape,
    IProfileFormShape, INewPasswordFormShape,
} from '../components/forms/types';
import { IPostModel, IProfileModel } from '../types/types';

// Instruments
import { AUTH_URL, FEED_URL } from './config';

export const api = {
    get token() {
        return localStorage.getItem('token') || '';
    },
    auth: {
        async signup(userInfo: ISignUp) {
            const { data } =  await axios.post(`${AUTH_URL}/register`,
                userInfo,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });

            return data;
        },
        async login(credentials: ILoginFormShape) {
            const { data } = await axios.post(`${AUTH_URL}/login`,
                credentials,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });

            return data;
        },
        async auth(): Promise<void> {
            await axios.get<void>(`${AUTH_URL}/auth`, {
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });
        },
        logout() {
            return fetch(`${AUTH_URL}/logout`, {
                method:  'GET',
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });
        },
    },
    posts: {
        async fetch():Promise<IPostModel[]> {
            const { data } = await axios.get<AxiosResponse<IPostModel[]>>(FEED_URL, {
                headers: {
                    Authorization: api.token,
                },
            });

            return data.data;
        },
        async create(post: IPostFormShape): Promise<IPostModel> {
            const { data } = await axios.post<IPostFormShape, AxiosResponse<IPostModel>>(FEED_URL,
                post,
                {
                    headers: {
                        Authorization: `Bearer ${api.token}`,
                    },
                });

            return data;
        },
        remove(postId: string) {
            return fetch(`${FEED_URL}/${postId}`, {
                method:  'DELETE',
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });
        },
        like(postId: string) {
            return fetch(`${FEED_URL}/${postId}/like`, {
                method:  'PUT',
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });
        },
        unlike(postId: string) {
            return fetch(`${FEED_URL}/${postId}/unlike`, {
                method:  'PUT',
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });
        },
        async comment(comment: ICommentFormShape, hash: string) {
            const { data } = await axios.put(`${FEED_URL}/${hash}/comment`,
                { body: comment.body },
                {
                    headers: {
                        Authorization: `Bearer ${api.token}`,
                    },
                });

            return data;
        },
        async getComments() {
            const { data } = await axios.get(`${FEED_URL}/comments`);

            return data;
        },
        async getPostById(postHash: string) {
            const { data } = await axios.get(`${FEED_URL}/${postHash}`);

            return data;
        },
    },
    profile: {
        async fetch(): Promise<IProfileModel> {
            const { data } = await axios.get<IProfileModel>(`${AUTH_URL}/profile`, {
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });

            return data;
        },
        updateProfile(profileInfo: IProfileFormShape): Promise<IProfileModel> {
            const data = fetch(`${AUTH_URL}/profile`, {
                method:  'PUT',
                headers: {
                    Authorization:  `Bearer ${api.token}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(profileInfo),
            })
                .then((response) => response.json())
                .then((resData) => resData)
                .catch((error) => error);

            return data;
        },
        async resetPassword(passwordData: INewPasswordFormShape) {
            const { data } = await axios.post(`${AUTH_URL}/reset-password`, passwordData, {
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });

            return data;
        },
    },
};
