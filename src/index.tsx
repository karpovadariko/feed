// Core
import { render } from 'react-dom';
import { QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';

// Styles
import 'react-toastify/dist/ReactToastify.css';
import './theme/init.scss';

// Instruments
import { queryClient } from './lib';
import { store } from './lib/redux/init/store';

// App
import { App } from './App';

render(
    <>
        <Provider store = { store }>
            <QueryClientProvider client = { queryClient }>
                <Router>
                    <App />
                </Router>
                <ReactQueryDevtools initialIsOpen = { false } />
            </QueryClientProvider>
        </Provider>
    </>,
    document.getElementById('root'),
);
