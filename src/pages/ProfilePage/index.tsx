// Core
import { FC } from 'react';

// Components
import { Navigation, FeedLayout } from '../../components';
import { ProfileForm } from '../../components/forms';
import { useUpdateProfile } from '../../hooks';
import { IProfileFormShape } from '../../components/forms/types';

export const ProfilePage: FC = () => {
    const updateProfile = useUpdateProfile();

    const profileSubmit = async (data: IProfileFormShape) => {
        await updateProfile.mutateAsync(data);
    };

    return (
        <FeedLayout>
            <Navigation />
            <ProfileForm profileSubmit = { profileSubmit } />
        </FeedLayout>
    );
};
