// Components
import { SignUpForm } from '../../components/forms';

export const SignUpPage: React.FC = () => {
    return (
        <SignUpForm />
    );
};
