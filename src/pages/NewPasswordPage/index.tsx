// Components
import { Navigation, FeedLayout } from '../../components';
import { NewPasswordForm } from '../../components/forms';

export const NewPasswordPage: React.FC = () => {
    return (
        <FeedLayout>
            <Navigation />
            <NewPasswordForm />
        </FeedLayout>
    );
};
