// Components
import {
    Posts, RecentComments, Navigation, FeedLayout,
} from '../../components';

export const Feed: React.FC = () => {
    return (
        <FeedLayout>
            <Navigation />
            <Posts />
            <RecentComments />
        </FeedLayout>
    );
};
