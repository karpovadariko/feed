// Components
import { LoginForm } from '../../components/forms';

export const LoginPage: React.FC = () => {
    return (
        <>
            <LoginForm />
        </>
    );
};
