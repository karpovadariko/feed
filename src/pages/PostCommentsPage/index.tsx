// Components
import { PostComments, Navigation, FeedLayout } from '../../components';

export const PostCommentsPage: React.FC = () => {
    return (
        <FeedLayout>
            <Navigation />
            <PostComments />
        </FeedLayout>
    );
};
