// Core
import {
    Routes,  Route, Outlet, Navigate,
} from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';

// Components
import {
    Feed, ProfilePage, PostCommentsPage, SignUpPage, LoginPage, NewPasswordPage,
} from './pages';
import { Footer } from './components';

// Hooks
import { useAuth } from './hooks';

// Instruments
import { getErrorMessage } from './lib/redux/selectors';
import { uiActions } from './lib/redux/actions';
import { useAppDispatch } from './lib/redux/init/store';

export const App = () => {
    const errorMessage = useSelector(getErrorMessage);
    const dispatch = useAppDispatch();

    const notify = (msg: string) => toast.error(msg);
    useAuth();

    useEffect(() => {
        if (errorMessage) {
            notify(errorMessage);
            dispatch(uiActions.resetErrorMessage());
        }
    }, [errorMessage]);

    return (
        <>
            <main>
                <ToastContainer />

                <Routes>
                    <Route path = '/feed' element = { <Outlet /> }>
                        <Route path = '' element = { <Feed /> }></Route>
                        <Route path = ':postId' element = { <PostCommentsPage /> }></Route>
                    </Route>
                    <Route path = '/profile' element = { <Outlet /> }>
                        <Route path = '' element = { <ProfilePage /> }></Route>
                        <Route path = 'new-password' element = { <NewPasswordPage /> }></Route>
                    </Route>
                    <Route path = '/login' element = { <LoginPage /> }></Route>
                    <Route path = '/signup' element = { <SignUpPage /> }></Route>
                    <Route path = '*' element = { <Navigate to = '/feed' replace /> }></Route>
                </Routes>
            </main>
            <Footer />
        </>
    );
};
