import { RootState } from '../init/store';

export const getAuthToken = (state: RootState): string => {
    return state.auth.token;
};

export const getIsAuthSuccess = (state: RootState): boolean => {
    return state.auth.authSuccess;
};
