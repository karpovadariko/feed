import { RootState } from '../init/store';

export const getCommentsPostId = (state: RootState): string => {
    return state.comments.selectedPostId;
};
