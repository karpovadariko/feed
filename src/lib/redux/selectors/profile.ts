import { RootState } from '../init/store';

export const getUserName = (state: RootState): string => {
    return state.profile.userName;
};

export const getIsFetching = (state: RootState): boolean => {
    return state.profile.isFetching;
};
