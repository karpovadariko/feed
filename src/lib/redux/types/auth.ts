export const authTypes = Object.freeze({
    SET_TOKEN:           'SET_TOKEN',
    SET_IS_AUTH_SUCCESS: 'SET_IS_AUTH_SUCCESS',
});
