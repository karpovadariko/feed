export const profileTypes = Object.freeze({
    SET_USER_NAME:  'SET_USER_NAME',
    START_FETCHING: 'START_FETCHING',
    STOP_FETCHING:  'STOP_FETCHING',
});
