import { uiTypes } from '../types';

export const uiActions = Object.freeze({
    setErrorMessage: (errorMsg: string) => {
        return {
            type:    uiTypes.SET_ERROR_MESSAGE,
            payload: errorMsg,
        };
    },
    resetErrorMessage: () => {
        return {
            type: uiTypes.RESET_ERROR_MESSAGE,
        };
    },
});
