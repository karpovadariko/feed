import { commentTypes } from '../types';

export const commentActions = Object.freeze({
    setCommentsPostId: (postId: string) => {
        return {
            type:    commentTypes.SET_COMMENTS_POST_ID,
            payload: postId,
        };
    },
});
