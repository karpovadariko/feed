import { profileTypes } from '../types';
import { authActions } from './auth';
import { uiActions } from './ui';
import { api } from '../../../api';
import { INewPasswordFormShape } from '../../../components/forms/types';
import { AppThunk } from '../init/store';

export const profileActions = Object.freeze({
    setUserName: (userName: string) => {
        return {
            type:    profileTypes.SET_USER_NAME,
            payload: userName,
        };
    },
    startFetching: () => {
        return {
            type: profileTypes.START_FETCHING,
        };
    },
    stopFetching: () => {
        return {
            type: profileTypes.STOP_FETCHING,
        };
    },
    resetPasswordAsync: (passwordData: INewPasswordFormShape): AppThunk => async (dispatch) => {
        try {
            dispatch(profileActions.startFetching());
            const { data: token } = await api.profile.resetPassword(passwordData);
            dispatch(authActions.setToken(token));
        } catch (error) {
            const { message } = error as Error;
            dispatch(uiActions.setErrorMessage(message));
        } finally {
            dispatch(profileActions.stopFetching());
        }
    },
});

