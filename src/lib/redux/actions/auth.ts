import { authTypes } from '../types';

export const authActions = Object.freeze({
    setToken: (token: string) => {
        return {
            type:    authTypes.SET_TOKEN,
            payload: token,
        };
    },
    setIsAuthSuccess: (isAuthSuccess: boolean) => {
        return {
            type:    authTypes.SET_IS_AUTH_SUCCESS,
            payload: isAuthSuccess,
        };
    },
});
