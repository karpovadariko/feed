// Core
import { combineReducers } from 'redux';

// Reducers
import {
    authReducer as auth,
    commentsReducer as comments,
    profileReducer as profile,
    uiReducer as ui,
} from '../reducers';

export const rootReducer = combineReducers({
    auth,
    comments,
    profile,
    ui,
});
