import { AnyAction } from 'redux';
import { authTypes } from '../types';

const initialState = {
    token:       '',
    authSuccess: false,
};

export const authReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case authTypes.SET_TOKEN: {
            return {
                ...state,
                token: action.payload,
            };
        }
        case authTypes.SET_IS_AUTH_SUCCESS: {
            return {
                ...state,
                authSuccess: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};
