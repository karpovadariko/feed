import { AnyAction } from 'redux';
import { profileTypes } from '../types';

const initialState = {
    userName:   '',
    isFetching: false,
};

export const profileReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case profileTypes.SET_USER_NAME: {
            return {
                ...state,
                userName: action.payload,
            };
        }
        case profileTypes.START_FETCHING: {
            return {
                ...state,
                isFetching: true,
            };
        }
        case profileTypes.STOP_FETCHING: {
            return {
                ...state,
                isFetching: false,
            };
        }
        default: {
            return state;
        }
    }
};
