import { AnyAction } from 'redux';
import { commentTypes } from '../types';

const initialState = {
    selectedPostId: '',
};

export const commentsReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case commentTypes.SET_COMMENTS_POST_ID: {
            return {
                ...state,
                selectedPostId: action.payload,
            };
        }
        default: {
            return state;
        }
    }
};
