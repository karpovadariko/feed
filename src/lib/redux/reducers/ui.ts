import { AnyAction } from 'redux';
import { uiTypes } from '../types';

const initialState = {
    message: '',
};

export const uiReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case uiTypes.SET_ERROR_MESSAGE: {
            return {
                ...state,
                message: action.payload,
            };
        }
        case uiTypes.RESET_ERROR_MESSAGE: {
            return {
                ...state,
                message: '',
            };
        }
        default: {
            return state;
        }
    }
};
