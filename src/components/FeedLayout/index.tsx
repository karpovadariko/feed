// Styles
import { FeedWrapper } from '../styled';

type Props = {
    children: React.ReactElement | React.ReactElement[]
};

export const FeedLayout: React.FC<Props> = ({ children }) => {
    return (
        <FeedWrapper>
            <div className = 'container'>
                { children }
            </div>
        </FeedWrapper>
    );
};
