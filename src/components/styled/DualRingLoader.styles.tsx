import styled from 'styled-components';

export const DualRingLoader = styled.div`
    display: inline-block;
    width: 100%;
    height: 80px;
    margin: 0 auto;
    text-align: center;

    &:after {
        content: " ";
        display: inline-block;
        width: 44px;
        height: 44px;
        margin: 8px;
        border-radius: 50%;
        border-width: 5px;
        border-style: solid;
        border-color: #dfc transparent #dfc transparent;
        animation: lds-dual-ring 1.2s linear infinite;
    }
`;
