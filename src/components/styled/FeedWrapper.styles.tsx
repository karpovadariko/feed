import styled from 'styled-components';

export const FeedWrapper = styled.div`
    display: flex; 

    & > div {
        flex: 1;

    }

    .container { 
        display: flex;
        justify-content: space-between;
        max-width: 1200px;
        width: 100%;
        margin: 0 auto;
        padding: 0 20px;
        flex-shrink: 0;
        position: relative;

        & > div {
            &:nth-child(1), &:nth-child(3) {
                width: 20%;
                box-sizing: border-box;
                padding-top: 50px;
                position: sticky;
                top: 0;
                height: 80vh;
            }
            &:nth-child(1) {
                padding-right: 30px;
            }
            &:nth-child(3) {
                width: 30%;
            }
        }
    }

    .posts-container {
        width: 100%;
    }

    .posts {
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        align-items: center;
        padding: 50px 0 10px;
        background-color: var(--paletteColor3);
    }

    .title {
        font-size: 24px;
        color: var(--paletteColor2);
        margin-bottom: 35px;
    }
`;
