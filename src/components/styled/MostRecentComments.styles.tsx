import styled from 'styled-components';

export const MostRecentComments = styled.div`
    padding: 10px 0 10px 10px;
    max-width: 550px;
    flex-shrink: 0;

    .comment {
        margin-bottom: 15px;
        position: relative;
        margin-top: 10px;
        width: 100%;
        flex-grow: 1;
        max-width: calc(100vw - 20px);
        padding: 12px;
        border: unset;
        background-color: var(--paletteColor11);
        border-radius: 10px;
        word-wrap: break-word;
        transition: box-shadow 0.3s ease;

        &:first-child{
            margin-top: 0;
        }

        .body {
            font-size: 20px;
            margin-bottom: 15px;
        }
        a {
            text-decoration: unset;
            color: var(--paletteColor5);
        }
        & > p:first-child {
        color: var(--paletteColor2);
        font-weight: 700;
        margin-bottom: 5px;
        }

        & > time {
            display: block;
            font-size: 12px;
            color: var(--paletteColor5);
            font-weight: normal;
            margin-bottom: 15px;
        }

        & .body, & > .comment {
            color: var(--paletteColor2);
            font-size: 20px;
        }

        & .comment {
            border-radius: unset;
            background-color: var(--paletteColor11);
            border-bottom: 1px solid var(--paletteColor2);

            &:last-child {
                border-bottom: unset;
            }
        }
    }

    section {
        overflow-y: auto;
        height: 100%;
    }

    & h1 {
        text-align: center;
    }

    .title {
        font-size: 24px;
        margin-bottom: 35px;
        text-align: center;
        color: white;
    }

    .subtitle {
        margin-top: 30px;
        margin-bottom: 20px;
        font-size: 18px;
        color: var(--paletteColor5)
    }
`;
