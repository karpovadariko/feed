import styled from 'styled-components';

// Images
import logoWhite from '../../theme/assets/logo-white.png';
import exit from '../../theme/assets/exit.png';
import profile from '../../theme/assets/profile.png';
import home from '../../theme/assets/home.png';

export const NavigationWrapper = styled.div`
    .logo {
        width: 115px;
        height: 36px;
        background: transparent url(${logoWhite}) center center no-repeat;
        background-size: contain;
        flex-shrink: 0;
    }

    .navigation {
        position: sticky;
        top: 0;
        z-index: 1;
        display: flex;
        align-items: center;
        justify-content: space-between;
        height: 50px;
        padding: 0 20px;
        background-color: var(--paletteColor6);

        & nav {
            display: flex;
            margin-left: auto;

            & a:first-child {
                margin-right: 5px;
            }
        }

        & a,
        & button {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 27px;
            padding: 2px 10px;
            font-size: 16px;
            font-weight: 500;
            color: var(--paletteColor2);
            text-decoration: none;
            white-space: nowrap;
            cursor: pointer;
            background-color: transparent;
            // border: 1px solid red;
            //border-color: color-mod(var(--paletteColor6) lightness(65%));
            // border-radius: 7px;
            border: unset;
            outline: 0;
            transition: background-color 0.3s ease, border-color;

            &:hover {
                background-color: var(--rgbaColor2);
                //border-color: color-mod(var(--paletteColor6) lightness(55%));
                border-color: red;
            }

            & img {
                width: 24px;
                height: 24px;
                margin-right: 10px;
                border: 1px solid var(--rgbaColor1);
                border-radius: 50%;
            }
        }

        & .active {
            background: var(--rgbaColor2);
        }

        & .hidden {
            flex-grow: 0;
            visibility: hidden;
            width: 0;
            overflow: hidden;
        }

        & .status {
            display: flex;
            align-items: center;
            justify-content: space-between;
            min-width: 80px;
            height: 24px;
            padding: 5px;
            margin-right: 5px;
            margin-left: 20px;
            font-size: 14px;
            font-weight: 500;
            color: #365899;
            cursor: pointer;
            user-select: none;
            background: #f4f6f8;
            // border-radius: 5px;

            & span {
                width: 9px;
                height: 9px;
                border-radius: 50%;
            }
        }

        & .online {
            & span {
                background: #578843;
                box-shadow: 0 0 2px #578843;
            }
        }

        & .offline {
            & span {
                background: #f40009;
                box-shadow: 0 0 2px #f40009;
            }
        }
    }

    .navigation-item {
        display: flex;
        align-items: center;
        color: white;
        text-decoration: unset;
        font-size: 20px;
        line-height: 22px;
        margin-bottom: 20px;
        
        &:hover {
            opacity: 0.8;
        }
        &::before {
            content: '';
            display: block;
            width: 26px;
            height: 26px;
            background: transparent url(${profile}) center center no-repeat;
            background-size: cover;
            margin-right: 10px;
        }

        &:nth-child(2) {
            &::before {
                background: transparent url(${home}) center center no-repeat;
                background-size: cover;

            }
        }
    }

    .navigation-item.active {
        font-weight: 700;
        position: relative;

        &::after{
            content: '';
            display: block;
            width: 100%;
            height: 200%;
            padding: 0 20px;
            border-radius: 50px;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translateX(-50%) translateY(-50%);
            z-index: -1;
            background: var(--paletteColor11);
        }
    }

    .navigation-profile {
        color: white;
        font-size: 24px;
        line-height: 26px;
        text-decoration: unset;
        font-weight: 700;
        margin-bottom: 30px;
        display: block;

        .profile-wrapper {
            position: relative;
        }
    }

    .navigation-avatar {
        width: 150px;
        height: 150px;
        display: block;
        margin-bottom: 20px;
        border-radius: 50%;
        background: var(--paletteColor5);
    }

    .user-status {
        position: absolute;
        left: 120px;
        bottom: 0;

        .status {
            span {
                display: block;
                width: 30px;
                height: 30px;
                border-radius: 50%;
            }

            &.online span{
                background: #578843;
                
            }
        
            &.offline span{
                background: #f40009;
                
            }
        }
    }

    .logout {
        position: absolute;
        bottom: 0;
        left: 0;
        color: var(--paletteColor5);
        background: unset;
        border: unset;
        font-size: 16px;
        transition: 0.3s all ease;
        cursor: pointer;
        display: flex;
        align-items: center;

        &::before{
            content: '';
            display: block;
            width: 26px;
            height: 26px;
            background: transparent url(${exit}) center center no-repeat;
            background-size: cover;
            margin-right: 10px;
        }

        &:hover {
            opacity: 0.7;
            transition: 0.3s all ease;
        }
    }
`;
