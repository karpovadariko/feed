import * as yup from 'yup';
import { ISignUpFormShape } from '../types';

const validEmailMsg = 'Введите валидный имейл';
// eslint-disable-next-line no-template-curly-in-string
const tooShortMsg = 'Минимальная длина ${min} символов';
const dontMatch = 'Пароли не совпадают';

export const schema: yup.SchemaOf<ISignUpFormShape> = yup.object({
    name: yup.string()
        .min(3, tooShortMsg)
        .required('*'),
    email: yup.string()
        .email(validEmailMsg)
        .required('*'),
    password: yup.string()
        .min(6, tooShortMsg)
        .required('*'),
    confirmPassword: yup.string()
        .min(6, tooShortMsg)
        .required('*')
        .oneOf([yup.ref('password')], dontMatch),
});
