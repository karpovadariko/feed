// Core
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';

// Elements
import { Input } from '../elements';

// Hooks
import { useSignUp } from '../../../hooks';

// Helpers
import { schema } from './config';
import { ISignUpFormShape } from '../types';

// Styles
import { Form } from '../../styled';

export const SignUpForm: React.FC = () => {
    const signUp = useSignUp();

    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (data: ISignUpFormShape) => {
        const { confirmPassword, ...userData } = data;
        await signUp.mutateAsync(userData);
    });

    return (
        <Form className = 'centered' onSubmit = { onSubmit }>
            <div className = 'wrapper centered'>
                <div className = 'logo'></div>
                <div>
                    <Input
                        placeholder = 'Имя'
                        register = { form.register('name') }
                        error = { form.formState.errors.name } />
                    <Input
                        placeholder = 'Почта'
                        register = { form.register('email') }
                        error = { form.formState.errors.email } />
                    <Input
                        type = 'password'
                        placeholder = 'Пароль'
                        register = { form.register('password') }
                        error = { form.formState.errors.password } />
                    <Input
                        type = 'password'
                        placeholder = 'Пароль'
                        register = { form.register('confirmPassword') }
                        error = { form.formState.errors.confirmPassword } />
                    <button className = 'signupSubmit' type = 'submit'>Создать аккаунт</button>
                </div>
                <p className = 'options'>Есть аккаунт?<Link to = '/login'>Войти</Link></p>
            </div>
        </Form>
    );
};
