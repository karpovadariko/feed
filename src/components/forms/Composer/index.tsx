// Styles
import { Composer } from '../../styled';

export const PostForm: React.FC = () => {
    return (
        <Composer>
            <img src = 'https://placeimg.com/256/256/animals' alt = 'avatar' />
            <form>
                <label>
                    <div>
                        <span className = 'error-message'></span>
                    </div>
                    <textarea placeholder = "What's on your mind, Elon Musk?" name = 'body'></textarea>
                </label>
                <button type = 'submit'>Запостить</button>
            </form>
        </Composer>
    );
};
