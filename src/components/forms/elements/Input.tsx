import { UseFormRegisterReturn } from 'react-hook-form';

type IPropTypes = {
    type?: string,
    placeholder: string,
    register: UseFormRegisterReturn,
    error?: {
        message?: string
    }
};

export const Input: React.FC<IPropTypes> = (props) => {
    const input = <input
        type = { props.type } { ...props.register }
        placeholder = { props.placeholder } />;

    return (
        <label>
            <div><span className = 'error-message'>{ props.error?.message }</span></div>
            { input }
        </label>
    );
};

Input.defaultProps = {
    type: 'text',
};
