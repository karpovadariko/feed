import * as yup from 'yup';
import { ILoginFormShape } from '../types';

const validEmailMsg = 'Введите валидный имейл';
// eslint-disable-next-line no-template-curly-in-string
const tooShortMsg = 'Минимальная длина ${min} символов';

export const schema: yup.SchemaOf<ILoginFormShape> = yup.object({
    email: yup.string()
        .email(validEmailMsg)
        .required('*'),
    password: yup.string()
        .min(6, tooShortMsg)
        .required('*'),
});
