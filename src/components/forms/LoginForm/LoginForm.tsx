// Core
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

// Components
import { Input } from '../elements';

// Hooks
import { useLogin } from '../../../hooks';

// Helpers
import { schema } from './config';
import { ILoginFormShape } from '../types';

// Styles
import { Form } from '../../styled';

export const LoginForm: React.FC = () => {
    const login = useLogin();

    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (data: ILoginFormShape) => {
        await login.mutateAsync(data);
    });

    return (
        <Form className = 'centered' onSubmit = { onSubmit }>
            <div className = 'wrapper centered'>
                <div className = 'logo'></div>
                <div>
                    <Input
                        placeholder = 'Почта' register = { form.register('email') }
                        error = { form.formState.errors.email } />
                    <Input
                        type = 'password' placeholder = 'Пароль'
                        register = { form.register('password') } error = { form.formState.errors.password } />
                    <button className = 'loginSubmit' type = 'submit'>Войти</button>
                </div>
                <p className = 'options'>Нет аккаунта?<Link to = '/signup'>Создать</Link>
                </p>
            </div>
        </Form>
    );
};
