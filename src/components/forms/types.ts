export interface INewPasswordFormShape {
    oldPassword: string,
    newPassword: string
}

export interface ISignUpFormShape {
    name: string,
    email: string,
    password: string,
    confirmPassword: string
}

export interface ISignUp extends Omit<ISignUpFormShape, 'confirmPassword'>{}

export interface IProfileFormShape {
    firstName: string,
    lastName: string
}

export interface ILoginFormShape {
    email: string,
    password: string
}

export interface IPostFormShape {
    body: string
}

export interface ICommentFormShape {
    body: string
}

