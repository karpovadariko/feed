import * as yup from 'yup';
import { INewPasswordFormShape } from '../types';

// eslint-disable-next-line no-template-curly-in-string
const tooShortMsg = 'Минимальная длина ${min} символов';

export const schema: yup.SchemaOf<INewPasswordFormShape> = yup.object({
    oldPassword: yup.string()
        .min(6, tooShortMsg)
        .required('*'),
    newPassword: yup.string()
        .min(6, tooShortMsg)
        .required('*'),
});
