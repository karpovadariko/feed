// Core
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

// Components
import { Input } from '../elements';

// Hooks
import { useNewPassword } from '../../../hooks';

// Instruments
import { schema } from './config';
import { profileActions } from '../../../lib/redux/actions';
import { INewPasswordFormShape } from '../types';
import { useAppDispatch } from '../../../lib/redux/init/store';

// Styles
import { Form } from '../../styled';

export const NewPasswordForm: React.FC = () => {
    useNewPassword();
    const dispatch = useAppDispatch();

    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (data: INewPasswordFormShape) => {
        await dispatch(profileActions.resetPasswordAsync(data));
        form.reset();
    });

    return (
        <Form onSubmit = { onSubmit }>
            <div className = 'wrapper'>
                <div>
                    <h1>Смена пароля</h1>
                    <Input
                        placeholder = 'Старый пароль'
                        register = { form.register('oldPassword') }
                        type = 'password'
                        error = { form.formState.errors.oldPassword } />
                    <Input
                        placeholder = 'Новый пароль'
                        register = { form.register('newPassword') }
                        type = 'password'
                        error = { form.formState.errors.newPassword } />
                    <button className = 'loginSubmit' type = 'submit'>Сменить пароль</button>
                </div>
                <Link to = '/profile'>← Назад</Link>
            </div>
        </Form>
    );
};
