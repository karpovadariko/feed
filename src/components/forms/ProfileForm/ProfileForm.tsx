// Core
import { FC, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useSelector } from 'react-redux';

// Components
import { Input } from '../elements';

// Hooks
import { useProfile } from '../../../hooks';

// Instruments
import { schema } from './config';
import { getIsAuthSuccess, getUserName } from '../../../lib/redux/selectors';
import { IProfileFormShape } from '../types';

// Styles
import { Form } from '../../styled';

type IPropTypes = {
    profileSubmit: (data: IProfileFormShape) => void
};

export const ProfileForm: FC<IPropTypes> = ({ profileSubmit }) => {
    const form = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (data: IProfileFormShape) => {
        await profileSubmit(data);
        form.reset();
    });

    const userName  = useSelector(getUserName);
    const isAuthSuccess  = useSelector(getIsAuthSuccess);
    const refetch = useProfile();

    useEffect(() => {
        if (isAuthSuccess) {
            (async () => {
                await refetch();
            })();
        }
    }, [isAuthSuccess]);

    return (
        <Form role = 'profileForm' onSubmit = { onSubmit }>
            <div className = 'wrapper'>
                <div>
                    <h1>Привет { userName }</h1>
                    <img src = 'https://placeimg.com/256/256/animals' alt = 'avatar' />
                    <Input
                        placeholder = 'Имя'
                        register = { form.register('firstName') }
                        error = { form.formState.errors.firstName } />
                    <Input
                        placeholder = 'Фамилия'
                        register = { form.register('lastName') }
                        error = { form.formState.errors.lastName } />
                    <button className = 'loginSubmit' type = 'submit'>Обновить профиль</button>
                </div>
                <Link to = '/profile/new-password'>Cменить пароль →</Link>
            </div>
        </Form>
    );
};
