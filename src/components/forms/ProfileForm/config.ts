import * as yup from 'yup';
import { IProfileFormShape } from '../types';

// eslint-disable-next-line no-template-curly-in-string
const tooShortMsg = 'Минимальная длина ${min} символов';

export const schema: yup.SchemaOf<IProfileFormShape> = yup.object({
    firstName: yup.string()
        .min(3, tooShortMsg)
        .required('*'),
    lastName: yup.string()
        .min(3, tooShortMsg)
        .required('*'),
});
