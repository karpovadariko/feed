// Core
import { Link, useParams } from 'react-router-dom';

// Hooks
import { usePostDetails } from '../../hooks';

// Helpers
import { timeSince, fetchify } from '../../helpers';
import { IPostComment } from '../../types/PostModel';

// Styles
import { PostCommentsWrapper } from '../styled';

export const PostComments: React.FC = () => {
    const params = useParams();
    const postId = params?.postId || '';
    const { data: post, isFetching } = usePostDetails(postId);

    const commentsJSX = post?.comments?.map((comment: IPostComment) => {
        const {
            hash, author, created, body,
        } = comment;

        return (
            <div className = 'comment' key = { hash }>
                <p className = 'name'>{ author?.name }</p>
                <time>{ timeSince(created) }</time>
                <p className = 'body'>{ body }</p>
            </div>
        );
    });

    return (
        <PostCommentsWrapper>
            <Link
                aria-current = 'page'
                className = 'link-back' to = '/feed'>
                <div className = 'arrow'></div>
                Назад
            </Link>
            <h1 className = 'title'>Комментарии к посту</h1>
            <section>
                <div className = 'comment'>
                    <p className = 'name'>{ fetchify(post?.author?.name, isFetching) }</p>
                    <time>{ fetchify(timeSince(post?.created), isFetching) }</time>
                    <p className = 'body'>{ fetchify(post?.body, isFetching) }</p>
                    <p className = 'subtitle'>Популярные комментарии</p>
                    { fetchify(commentsJSX, isFetching) }
                </div>
            </section>
        </PostCommentsWrapper>
    );
};
