// Core
import { FC } from 'react';

// Components
import { ResentComment } from '../ResentComment';

// Hooks
import { useResentComments } from '../../hooks';

// Helpers
import { fetchify } from '../../helpers';
import { ICommentModel } from '../../types/CommentModel';

// Styles
import { MostRecentComments } from '../styled';

export const RecentComments: FC = () => {
    const { data, isFetching, isError } = useResentComments();
    const commentsJSX = data.map((comment: ICommentModel) => {
        return (
            <ResentComment key = { comment.hash } { ...comment } />
        );
    });

    return (
        <MostRecentComments>
            <h1 className = 'title'>Популярные комментарии</h1>
            { isError && <h2>Произошла ошибка, попробуйте позже...</h2> }
            <section role = 'recent-comments-section'>
                { fetchify(commentsJSX, isFetching) }
            </section>
        </MostRecentComments>
    );
};
