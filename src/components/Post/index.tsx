// Core
import { useSelector } from 'react-redux';

// Icons
import { LikeIcon } from '../../theme/assets/like';
import { CommentIcon } from '../../theme/assets/comment';

// Components
import { CommentsForm } from '../forms';
import { Comment } from '../Comment';

// Helpers
import { timeSince } from '../../helpers';

// Instruments
import { getCommentsPostId } from '../../lib/redux/selectors';
import { commentActions } from '../../lib/redux/actions';
import { IPostComment, IPostModel } from '../../types/PostModel';
import { useAppDispatch } from '../../lib/redux/init/store';

// Styles
import { Like, PostWrapper } from '../styled';

export const Post: React.FC<IPostModel> = (props) => {
    const {
        author, body, created, likes, comments, hash,
    } = props;
    const commentsPostId = useSelector(getCommentsPostId);
    const dispatch = useAppDispatch();

    const commentsJSX = comments.map((comment: IPostComment) => {
        const { hash: commentHash } = comment;

        return (
            <Comment key = { commentHash } { ...comment } />
        );
    });

    const handleClick = () => {
        const id = commentsPostId === hash ? '' : hash;
        dispatch(commentActions.setCommentsPostId(id));
    };

    return (
        <PostWrapper>
            <img src = { author.avatar } alt = 'avatar' />
            <a>{ author.name }</a>
            <time>{ timeSince(created) }</time>
            <p>{ body }</p>
            <div className = 'reaction-controls'>
                <Like>
                    <div>
                        <span>{ likes.length }</span>
                    </div>
                    <span className = 'icon'>
                        <LikeIcon className = 'like-icon' />Like
                    </span>
                </Like>
                <span className = 'comment' onClick = { handleClick }>
                    <CommentIcon className = 'comment-icon' />{ comments.length } comments
                </span>
            </div>

            { commentsPostId === hash
            && <>
                <CommentsForm />
                <ul>
                    { commentsJSX }
                </ul>
            </>
            }
        </PostWrapper>
    );
};
