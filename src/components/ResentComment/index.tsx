// Core
import { Link } from 'react-router-dom';

// Helpers
import { timeSince } from '../../helpers';
import { ICommentModel } from '../../types/CommentModel';

export const ResentComment: React.FC<ICommentModel> = (props) => {
    const {
        body, author, created, post,
    } = props;

    return (
        <div className = 'comment'>
            <p className = 'name'>{ author.name }</p>
            <time>{ timeSince(created) }</time>
            <p className = 'body'>{ body }</p>
            <Link to = { `/feed/${post?.hash}` }>Больше комментариев к посту</Link>
        </div>
    );
};
