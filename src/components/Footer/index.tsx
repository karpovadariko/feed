export const Footer: React.FC = () => {
    return (
        <footer className = 'footer'>
            <p>&copy; Lectrum LLC { new Date().getFullYear() }. Версия: 1.0.10</p>
        </footer>
    );
};
