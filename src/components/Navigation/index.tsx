// Core
import { FC, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';

// Instruments
import { useProfile } from '../../hooks';
import { getIsAuthSuccess, getUserName } from '../../lib/redux/selectors';

// Styled
import { NavigationWrapper } from '../styled';

export const Navigation: FC = () => {
    const  userName  = useSelector(getUserName);
    const  isAuthSuccess  = useSelector(getIsAuthSuccess);
    const refetch  = useProfile();

    useEffect(() => {
        if (isAuthSuccess) {
            (async () => {
                await refetch();
            })();
        }
    }, [isAuthSuccess]);

    return (
        <NavigationWrapper>
            <div className = 'navigation-profile'>
                <div className = 'profile-wrapper'>
                    <img
                        alt = '' className = 'navigation-avatar'
                        src = 'https://placeimg.com/256/256/animals' />
                    <div className = 'user-status'>
                        <div className = 'status online'>
                            <span></span>
                        </div>
                    </div>
                </div>
                { userName }
            </div>
            <NavLink
                className = 'navigation-item'
                to = '/profile'>Профиль</NavLink>
            <NavLink
                className = 'navigation-item'
                aria-current = 'page'
                to = '/feed'>Стена</NavLink>
            <button className = 'logout'>Выйти</button>
        </NavigationWrapper>
    );
};
