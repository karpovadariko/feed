// Core
import { FC } from 'react';

// Helpers
import { timeSince } from '../../helpers';
import { IPostComment } from '../../types/PostModel';

export const Comment: FC<IPostComment> = (props) => {
    const { body, author, created } = props;

    return (
        <li role = 'comment-body' className = 'commentBody'>
            <p>{ author.name }<span>{ timeSince(created) }</span></p>
            <p>{ body }</p>
        </li>
    );
};
