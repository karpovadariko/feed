/* eslint-disable node/no-unpublished-import */
// @ts-nocheck
import { screen } from '@testing-library/react';
import { Comment } from '../Comment';
import { renderWithClient } from '../../tests/utils';

const fakeProps =  {
    hash:   'effekef232jfejfj24',
    body:   'I agree with you',
    author: {
        hash: 'fjfrjfefej233223j',
        name: 'Jake',
    },
    created: Date.now(),
};

describe('Comment', () => {
    it('should render comment body', () => {
        renderWithClient(<Comment { ...fakeProps } />);
        expect(screen.getByRole('comment-body')).toBeInTheDocument();
    });

    it('snapshot', () => {
        const { container } = renderWithClient(<Comment { ...fakeProps } />);
        expect(container).toMatchSnapshot();
    });
});
