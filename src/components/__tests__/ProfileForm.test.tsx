// @ts-nocheck
/* eslint-disable node/no-unpublished-import */

import React from 'react';
import {
    screen, fireEvent, waitFor,
} from '@testing-library/react';
import '@testing-library/jest-dom';

// Instruments
import { renderWithClient } from '../../tests/utils';
import { ProfileForm } from '../forms';

const init = (func = jest.fn()) => {
    const { result, rerender } = renderWithClient(
        <ProfileForm profileSubmit = { func } />,
    );

    return {
        result,
        rerender,
    };
};

describe('ProfileForm', () => {
    it('should render the basic form fields', () => {
        const mockOnSubmit = jest.fn();
        init(mockOnSubmit);
        expect(screen.getByPlaceholderText('Имя')).toBeInTheDocument();
        expect(screen.getByPlaceholderText('Фамилия')).toBeInTheDocument();
        expect(screen.getByRole('button', { name: /Обновить профиль/i })).toBeInTheDocument();
    });

    it('should call submit function with valid data', async () => {
        const mockOnSubmit = jest.fn();
        init(mockOnSubmit);

        const firstName = screen.getByPlaceholderText('Имя');
        const lastName = screen.getByPlaceholderText('Фамилия');
        const form = screen.getByRole('profileForm');
        const firstNameValue = 'Chuck';
        const lastNameValue = 'Norris';

        fireEvent.input(firstName, {
            target: {
                value: firstNameValue,
            },
        });

        fireEvent.input(lastName, {
            target: {
                value: lastNameValue,
            },
        });

        fireEvent.submit(form);

        await waitFor(() => {
            expect(mockOnSubmit).toBeCalledWith({
                firstName: firstNameValue,
                lastName:  lastNameValue,
            });
        });
    });
});

