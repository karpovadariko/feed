// @ts-nocheck
/* eslint-disable node/no-unpublished-import */

import React from 'react';
import { rest } from 'msw';
import { screen } from '@testing-library/react';
import { renderWithClient } from '../../tests/utils';
import { RecentComments } from '..';
import { server } from '../../setupTests';

describe('RecentComments', () => {
    it('should have header element', () => {
        renderWithClient(<RecentComments />);
        const heading = screen.getByRole('heading', { level: 1, name: /Популярные комментарии/i });
        expect(heading).toBeInTheDocument();
    });

    it('should display error if the request is failed', async () => {
        server.use(
            rest.get('*', (req, res, ctx) => {
                return res(ctx.status(500));
            }),
        );

        const result = renderWithClient(<RecentComments />);

        expect(await result.findByText(/Произошла ошибка, попробуйте позже.../i)).toBeInTheDocument();
    });
});

