// Components
import { Post } from '../Post';
import { PostForm } from '../forms';

// Hooks
import { usePosts } from '../../hooks';

// Helpers
import { fetchify } from '../../helpers';

export const Posts: React.FC = () => {
    const { data, isFetching } = usePosts();
    const postsJSX = data.map((post) => {
        return (
            <Post key = { post.hash } { ...post } />
        );
    });

    return (
        <div className = 'posts'>
            <h1 className = 'title'>Стена</h1>

            <PostForm />

            <div className = 'posts-container' style = { { position: 'relative' } }>
                { fetchify(postsJSX, isFetching) }
            </div>
        </div>
    );
};
