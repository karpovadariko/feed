export interface IPostComment {
    hash: string,
    body: string,
    author: {
        hash: string,
        name: string
    },
    created: Date
}

export interface IPostLike {
    hash: string,
    name: string
}

export interface IPostModel {
    hash: string,
    body: string,
    author: {
        hash: string,
        name: string,
        avatar: string
    },
    comments: IPostComment[],
    likes: IPostLike[],
    created: Date
}

