export interface ICommentModel {
    hash: string,
    body: string,
    author: {
        hash: string,
        name: string
    },
    created:string,
    post: {
        hash: string
    }
}

