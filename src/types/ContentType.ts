import { ReactElement } from 'react';

export type ContentType = string | undefined | null | boolean | ReactElement[];

export type DateType = string | Date;
