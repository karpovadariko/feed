export interface IProfileModel {
    data: {
        hash: string,
        name: string,
        email: string,
        avatar: string
    }
}

export interface IUpdateProfileModel {
    data: {
        firstName: string,
        lastName: string
    }
}

