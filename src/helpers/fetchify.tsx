import { DualRingLoader } from '../components/styled';
import { ContentType } from '../types/ContentType';

export const fetchify = (data: ContentType, isFetching: boolean) => {
    if (isFetching) {
        return (
            <DualRingLoader />
        );
    }

    return (
        data
    );
};
