// @ts-nocheck
import { fetchify } from '..';
import { DualRingLoader } from '../../components/styled';

const loader = <DualRingLoader />;
const data = 'some data';

describe('fetchfy', () => {
    it('should test if fetching is in progress', () => {
        expect(fetchify(data, true)).toEqual(loader);
    });

    it('should test if fetching is not in progress', () => {
        expect(fetchify(data, false)).toEqual(data);
    });
});
