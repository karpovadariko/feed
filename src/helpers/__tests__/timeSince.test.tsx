// @ts-nocheck
import { timeSince } from '..';

const aMinuteAgo = new Date(Date.now() - 1000 * 60);

describe('timeSince', () => {
    it('return formatted date', () => {
        expect(timeSince(aMinuteAgo)).toEqual('1 minute ago');
    });
});
