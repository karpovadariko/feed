import { formatDistance } from 'date-fns';
import { DateType } from '../types/ContentType';

export const timeSince = (created: DateType) => {
    const dateCreated = typeof created === 'string' ? new Date(created) : created;

    return formatDistance(dateCreated, Date.now(), { addSuffix: true });
};

